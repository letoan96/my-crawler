class MainsController < ApplicationController
  require 'open-uri'
  require 'json'
  require 'httparty'

  def index
    
    browser = Watir::Browser.new :safari ## Watir is web application testing framework based on Selenium
    browser.goto 'https://shop.adidas.jp/item/?cat1Id=08&cateId=1&condition=4%245&gendId=m&limit=120&page=1&price=1500-4500'
    puts "Waiting for page fully loaded..."
    sleep 1 ## wait till the targeted page is fully loaded
    doc = Nokogiri::HTML.parse(browser.html) ## crawl


    main_url = 'https://shop.adidas.jp'

  	product_urls = doc.xpath('//div[@class="itemCardArea clearfix test-itemCardArea"]/div[@class="itemCard"]/div/div/a/@href').first(5)
  	product_image_urls_1 = doc.xpath('//div[@class="itemCardArea clearfix test-itemCardArea"]/div[@class="itemCard"]/div/div/a/img[@class="itemImage"][1]/@src').first(5)
    product_image_urls_2 = doc.xpath('//div[@class="itemCardArea clearfix test-itemCardArea"]/div[@class="itemCard"]/div/div/a/img[@class="itemImage"][2]/@src').first(5)
    product_name_text = doc.xpath('//div[@class="itemCardArea clearfix test-itemCardArea"]/div[@class="itemCard"]/div/div/div[@class="description"]/a/span/text()').first(5)
    product_price = doc.xpath('//div[@class="itemCardArea clearfix test-itemCardArea"]/div[@class="itemCard"]/div/div/div[@class="description"]/div/div/p[1]/span/text()').first(5)
  	product_sale_price = doc.xpath('//div[@class="itemCardArea clearfix test-itemCardArea"]/div[@class="itemCard"]/div/div/div[@class="description"]/div/div/p[2]/span/text()').first(5)

  	product_links = product_urls.map{|url| main_url+url}
  	image_links_1 = product_image_urls_1.map{|url| url }
    image_links_2 = product_image_urls_2.map{|url| url }
  	product_names = product_name_text.map{|n| n }
    product_origin_price = product_price.map{|n| n.to_s.split(',').join('.').to_f }
    product_sale_price = product_sale_price.map{|n| n.to_s.split(',').join('.').to_f }

  	puts product_links.count
    puts image_links_1.count
    puts product_names.count

    html_data = open('https://www.vietcombank.com.vn/ExchangeRates/').read
    doc = Nokogiri::HTML(html_data)
    @jpy_rate = doc.xpath('//table[@class="tbl-01 rateTable"]/tr[td[text()="JPY"]]/td[5]').to_s.match(/\d+\.\d+/)[0].to_f
  
    @products = []
    5.times do |n|
      vnd_price = (@jpy_rate * product_sale_price[n].to_f + 100 + 150).to_i
      product_hash = {"link" => product_links[n], "name" => product_names[n], "image_1" => image_links_1[n], "image_2" => image_links_2[n], "origin_price" => product_origin_price[n],"sale_price" => product_sale_price[n], "vnd_price" => vnd_price } 
      @products.push(product_hash)
    end
    # byebug
    
  end

  def post_to_facebook
    
    access_token = "EAACEdEose0cBAO4zmf37sb0kDPO3vyxwSXHBKZA5zBUPZCMhczY20vzaGBWZBC0IXEko3D0zfc3T5vWjwH3hnLv6ZBFomSBuzr8KVQOlfy4ulRGHbpvrD4oxbq7WB3Lk2tFzy8zuZApZBOMZBmyJ8hZCV363CNkEO4ZBrI9M4vJqQByZCfcHGvDwRQQB9ZAOjNUnYLxOtFZBlOtF4wZDZD"
    url = "https://graph.facebook.com/v2.12/222625248491326/feed?access_token=" + access_token
    @products = params[:products]
    @products.each do |product|
      price = product['vnd_price'].to_i
      message = "Giày Adidas chính hãng " + product['name'] + ". Giá gốc " + product['origin_price'] + " Yên, sale còn " + product['sale_price'] +  " Yên. \nGiá về TP Hồ Chi Minh: #" + price  + ".000 VNĐ. \nOrder hàng về trong vòng 10 ngày. Click vào link để xem và chọn size."
      puts message
      link = product['link']
      options = { body:
        {
          message: message,
          link: link,
          published: false,
          scheduled_publish_time: (Time.now + 7.hours)
        }
      }
      result = HTTParty.post(url, options)
      puts result 
    end    
  end

end
