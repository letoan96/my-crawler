Rails.application.routes.draw do
  root 'mains#index'
  post 'post_to_facebook', to: 'mains#post_to_facebook', as: 'post_to_facebook'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
